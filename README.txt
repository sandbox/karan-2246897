
--------------------------------------------------------------------------------
                              Node Author
--------------------------------------------------------------------------------

Maintainers: 
 * Praveen Karan (mudi..), praveenkaran.engg@gmail.com


This modules shows a author information/profile of the current node..

Installation
-------------

 * Install/ Enable "Profile 2" and "Node Author" module.
 * Configure it on the blocks admin page at admin/build/blocks --- select profile and its fields.
 * Although the block will automatically only show on nodes, you should configure it to only show on the nodes you want - 
  eg only on blog content types or certain URLs.

Features
------------

1. "Node Author" module create a block that enabled you select one profile among multiple profile types
  created by Profile2 module or exiting drupal user account.

2. you can also be modify selected data/information from node author configuration page.
  "Node Author" module create a Author information block so you can also control 
  which content type will able to view this author information.

3. Module integrates well with Profile2 module - thus you can select one profile among multiple profile
  as mentioned in point 1

4. Using this module you can select required fields from selected profile to display on node author block.


Permissions
------------

The Node Author module include permissions for add required profile type or default user account to show in node author block. All of these permissions can be found at admin/people/permissions.


Recommended Module
--------------------

* Profile2 (http://drupal.org/project/profile2):  Enabling this module allows you to include (in a node author block) custom author information fields.


Related Module
--------------------
Node author (6.x)
This module is only supported ofr drupal 6.x version.In my module is integrated with profile2 module to fetch all any profile fields to display in node author block.
